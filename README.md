![WebVR Icon](https://gitlab.com/BadToxic/webvr-demo/raw/master/Assets/WebGLTemplates/WebVR/thumbnail.png)
# WebVR Demo

Minimal Unity WebVR demo project via WebGL using the WebVR Assets from the Unity Asset Store, provided by [BadToxic](http://badtoxic.de/wordpress)

See the project in action via [WebGL](https://badtoxic.gitlab.io/webvr-demo)

Get the original assets: https://assetstore.unity.com/packages/templates/systems/webvr-assets-109152

# About BadToxic:

- Discord (BadToxic Server) https://discord.gg/8QMCm2d
- Instagram BadToxic https://www.instagram.com/xybadtoxic
- Instagram GameDev https://www.instagram.com/badtoxicdev
- TikTok https://www.tiktok.com/@badtoxic
- Twitter https://twitter.com/BadToxic
- Apple Developer https://itunes.apple.com/us/developer/michael-groenert/id955473699
- GooglePlay https://play.google.com/store/apps/developer?id=BadToxic

- More: Linktree https://linktr.ee/badtoxic